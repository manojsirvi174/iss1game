import pygame
import math
pygame.init()
white = (255, 255, 255)
red = (255, 0, 0)
black = (0, 0, 0)                 # defing the color
blue = (0, 0, 255)
brown = (150, 75, 0)
yellow = (255, 255, 0)
velocity_x = 10
x = 600
y = 860
k = 0
vel = 10
h = [0, 300, 600, 900]
h2 = [50, 330, 650, 950]
h1 = [150, 350, 750, 1150]            # defining the x cordinate for moving obstacles
h3 = [150, 350, 700, 1000]
h4 = [1050, 640, 340, 80]
h5 = [100, 320, 610, 980]
d = []
p = 0
clock1=0
clock2=0
clock3=0
clock4=0
player1counter = 0
player2counter = 0
level = 1
speed = 20
timer1 = 0
timer2 = 0
scorey = [750, 710, 600, 560, 440, 400, 290, 250, 140, 100]
scorey1 = [100, 140, 250, 290, 400, 440, 560, 600, 710, 750]
minim = 900
score = 0
yarr = [70, 200, 360, 510, 650, 800]
screen_width = 1200
screen_height = 900
gameWindow = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("river game")
flagpr = 0
count = 0
second1 = 0
second2 = 0
image = pygame.image.load("shark.gif")
image1 = pygame.image.load("4.png")
rivercolor = (134, 160, 230)
image = pygame.transform.scale(image, (40, 40))
image1 = pygame.transform.scale(image1, (40, 40))
player1 = pygame.image.load("spaceship1.png")
player1 = pygame.transform.scale(player1, (40, 40))           # taking the image and resize them
player2 = pygame.image.load("spaceshipinvert.png")
player2 = pygame.transform.scale(player2, (40, 40))
stabletree = pygame.image.load("tree.png")
stabletree = pygame.transform.scale(stabletree, (40, 40))
flag1 = 1
flag2 = 0
tree2 = 0
tree1 = 0
win1 = 0
win2 = 0
wincheker = 0
exit_game = False
game_over = False


def hun1(var, h1, h2):
    gameWindow.blit(var, (h1, h2))


def distance(ex, px, ey, py):
    distance1 = math.sqrt((math.pow(ex - px, 2)) + (math.pow(ey - py, 2)))
    #print(distance1)
    if distance1 < 25:
        return True
    else:
        return False


font = pygame.font.SysFont(None, 40)


def message(msg, color):
    screen_text = font.render(msg, True, color)
    gameWindow.blit(screen_text, [600, 860])


def message1(msg, color):
    screen_text = font.render(msg, True, color)
    gameWindow.blit(screen_text, [600, 0])


def topmessage(text1):

    gameWindow.blit(text1, (0, 0))


def bottommessage(text1):
    gameWindow.blit(text1, (1080, 0))


def message3(msg, color):
    screen_text = font.render(msg, True, color)
    gameWindow.blit(screen_text, [500, 420])


def message4(msg, color):
    screen_text = font.render(msg, True, color)
    gameWindow.blit(screen_text, [1150, 0])


def restart(msg, color):
    screen_text = font.render(msg, True, color)
    gameWindow.blit(screen_text, [470, 500])


def middlemessage1(text1):
    gameWindow.blit(text1, (400, 0))


def middlemessage2(text1):
    gameWindow.blit(text1, (750, 0))
